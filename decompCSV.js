// Import des modules nécessaires
const fs = require("fs");
const readline = require("readline");
const path = require("path");

const csvFilePath = "C:\\Users\\noedo\\Downloads\\StockEtablissement_utf8.csv";
const outputDir = "./output/";
const maxFileSize = 500 * 1024 * 1024;

async function decomposeAndCleanCSV(csvFilePath, outputDir, maxFileSize) {
  const fileStream = fs.createReadStream(csvFilePath);
  
  // Création d'une interface de lecture de lignes pour le flux de fichier
  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity,
  });

  let currentFileSize = 0;
  let fileNumber = 1;
  let outputFileStream = null;
  
  for await (const line of rl) {
    if (!outputFileStream || currentFileSize + line.length > maxFileSize) {
      if (outputFileStream) {
        outputFileStream.end();
      }
      // Création d'un nouveau fichier de sortie avec un nom unique
      const outputFileName = path.join(
        outputDir,
        `output_${fileNumber}.csv`
      );
      // Création du flux de sortie pour le nouveau fichier
      outputFileStream = fs.createWriteStream(outputFileName);
      fileNumber++;
      currentFileSize = 0;
    }

    outputFileStream.write(line + "\n");
    currentFileSize += line.length;
  }

  if (outputFileStream) {
    outputFileStream.end();
  }
}

// Appel de la fonction avec les paramètres appropriés
decomposeAndCleanCSV(csvFilePath, outputDir, maxFileSize)
  .then(() => {
    console.log("Décomposition et nettoyage terminés avec succès.");
  })
  .catch((err) => {
    console.error(
      "Erreur lors de la décomposition et du nettoyage du fichier CSV :",
      err
    );
  });
