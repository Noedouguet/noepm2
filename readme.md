# Les diagrammes
Les diagrammes ce retrouvent dans le dossier imageDiagramme et à la fin du readme

# L'nstallation
- Pour commencer `npm install`.
- Créer un dossier output.
- Ne pas oublier d'avoir le fichier `StockEtablissement_utf8.csv` sur son pc et indiqué son emplacement.

# Processus d'indexation

Nous nous trouvons dans une application pour le processus d'indexion des données. Pour cela on a eu un fichier CSV qu'on a du découper pour l'insérer dans une base de donnée NoSQL MongoDB.

## Étapes d'exécution des scripts :

### 1. Décomposition et nettoyage du fichier CSV :
   - Utilisation de la fonction `decomposeAndCleanCSV` pour décomposer le fichier CSV volumineux en fragments plus petits et nettoyer les données vides.
   - Exécuter la commande `node ./decomposeCSV.js`
   - Les données décomposées sont écrites dans des fichiers de sortie dans le répertoire `output/`.

### 2. Lancement de MongoDB en local :
   - Utilisation de la commande `"C:\Program Files\MongoDB\Server\7.0\bin\mongod.exe" --dbpath=C:\data\db` pour démarrer le serveur MongoDB en local.

### 3. Indexation des fragments du fichier CSV dans MongoDB :
   - Un fichier CSV décomposé est attribué à chaque worker pour traitement.
   - Chaque worker lance le script `worker.js`, qui lit le fragment CSV assigné, nettoie les données et les insère dans la base de données MongoDB.
   - Les workers communiquent la progression et les erreurs via des messages.
   - L'index.js coordonne la répartition des fragments CSV entre les workers et surveille leur progression.
   - Pour lancer le processus d'indexation utiliser la commande `pm2 start process.json`

### 4. Mettre en pause et reprendre l'indexation
   - Pour mettre en pause exécuter la commande : `pm2 sendSignal SIGUSR1 indexation`.
   - Pour reprendre exécuter la commande : `pm2 sendSignal SIGUSR2 indexation`.

## Explication rapide du processus :

1. **Décomposition du fichier CSV :**
   - Le script `decomposeAndCleanCSV` lit le fichier CSV ligne par ligne.
   - Il découpe le fichier en fragments plus petits si la taille maximale est dépassée.
   - Les lignes vides sont supprimées et les données sont écrites dans des fichiers de sortie.

2. **Lancement de MongoDB en local :**
   - MongoDB est lancé localement en utilisant une commande spécifique.

3. **Indexation des données dans MongoDB :**
   - Le fichier CSV décomposé est divisé entre plusieurs workers.
   - Chaque worker lit son fragment assigné, nettoie les données et les insère dans MongoDB.
   - Les workers communiquent leur progression et les erreurs rencontrées.

En suivant ces étapes, le processus d'indexation des données CSV dans MongoDB est automatisé et distribué pour gérer de grands ensembles de données de manière efficace.

![Diagramme processus d'indexation](./imageDiagramme/diagrammeDactivite.PNG)
![Diagramme d'activité](./imageDiagramme/processusIndexion.PNG)