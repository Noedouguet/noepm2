// mongoHandler.js

let isPaused = false; // Variable pour suivre l'état de la pause

function pauseIndexing() {
    isPaused = true;
}

function resumeIndexing() {
    isPaused = false;
}

function isIndexingPaused() {
    return isPaused;
}

module.exports = { pauseIndexing, resumeIndexing, isIndexingPaused };