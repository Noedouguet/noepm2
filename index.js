const { Worker } = require("worker_threads");
const fs = require("fs");
const { pauseIndexing, resumeIndexing, isIndexingPaused } = require("./mongoHandler");

const numWorkers = 4;
const numCsv = fs.readdirSync("./output").length;
const numCsvPerWorker = Math.ceil(numCsv / numWorkers);

// Surveiller les signaux
process.on('SIGINT', () => {
  controlWorkers('pause');
  process.exit(0);
});

// Si le processus reçoit le signal "pause"
process.on('pause', () => {
  controlWorkers('pause');
});

// Si le processus reçoit le signal "reprise"
process.on('resume', () => {
  controlWorkers('resume');
});

// Attribution des fichiers CSV à chaque worker
const workerCsv = {};
for (let i = 0; i < numWorkers; i++) {
    workerCsv[`worker${i + 1}`] = [];
}

// Répartition des fichiers CSV entre les workers
let workerIndex = 1;
let csvIndex = 1;
for (const csvFile of fs.readdirSync("./output")) {
    workerCsv[`worker${workerIndex}`].push(csvFile);
    if (csvIndex % numCsvPerWorker === 0) {
        workerIndex++;
    }
    csvIndex++;
}

// Création et démarrage des workers
const workers = [];
for (let i = 0; i < numWorkers; i++) {
    const worker = new Worker("./worker.js", {
        workerData: {
            workerId: i + 1,
            workerCsv: workerCsv[`worker${i + 1}`],
        },
    });
    workers.push(worker);
    worker.on("message", (message) => {
        if (message.type === "progress") {
            console.log(
                `Progression worker: ${message.workerId}: ${message.progress} fichier: ${message.csvFile}`
            );
        } else if (message.type === "error") {
            console.error(`Erreur worker: ${message.workerId}: ${message.error}`);
        }
    });
}

// Fonction pour arrêter et reprendre les workers
function controlWorkers(action) {
    if (action === "pause") {
        pauseIndexing();
        console.log("Indexation en pause...");
        workers.forEach(worker => worker.postMessage({ type: "pause" }));
    } else if (action === "resume") {
        resumeIndexing();
        console.log("Reprise de l'indexation...");
        workers.forEach(worker => worker.postMessage({ type: "resume" }));
    }
}