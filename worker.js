// worker.js

const fs = require("fs");
const csv = require("csv-parser");
const { MongoClient } = require("mongodb");
const { parentPort, workerData } = require("worker_threads");
const {
  isIndexingPaused,
  pauseIndexing,
  resumeIndexing,
} = require("./mongoHandler");

const mongoUrl = "mongodb://127.0.0.1:27017";
const dbName = "ensitech";
const collectionName = "etablissements";

const { workerId, workerCsv } = workerData;

console.log(
  `Worker: ${workerId} prêt.`
);

async function indexCSVFragment() {
  const client = new MongoClient(mongoUrl, { useUnifiedTopology: true });

  try {
    await client.connect();
    const db = client.db(dbName);
    const collection = db.collection(collectionName);

    for (const csvFile of workerCsv) {
      if (!isIndexingPaused()) {
        // Vérifier si l'indexation est en pause
        console.log(`Traitement fichier CSV : ${csvFile}`);
        const stream = fs.createReadStream(`output/${csvFile}`).pipe(csv());

        const bulkOps = [];
        for await (const data of stream) {
          const requiredFields = [
            "siren",
            "nic",
            "siret",
            "dateCreationEtablissement",
            "dateDernierTraitementEtablissement",
            "typeVoieEtablissement",
            "libelleVoieEtablissement",
            "codePostalEtablissement",
            "libelleCommuneEtablissement",
            "codeCommuneEtablissement",
            "dateDebut",
            "etatAdministratifEtablissement",
          ];

          const document = {};

          for (const field of requiredFields) {
            if (data[field]) {
              document[field] = data[field];
            }
          }

          if (Object.keys(document).length > 0) {
            bulkOps.push({
              insertOne: {
                document,
              },
            });
          }
        }

        if (bulkOps.length > 0) {
          await collection.bulkWrite(bulkOps, { ordered: false }); // Autoriser les doublons
        }

        console.log(
          `Fichier: ${csvFile} traité par worker: ${workerId}`
        );
        parentPort.postMessage({
          type: "progress",
          workerId,
          progress: "100%",
          csvFile,
        });
      } else {
        console.log(`Pause sur worker ${workerId}`);
        // Mettre en attente pendant que l'indexation est en pause
        await new Promise((resolve) => setTimeout(resolve, 1000));
      }
    }
  } catch (err) {
    console.error(
      `Erreur du traitement du fragment ${workerId} fichier: ${err}`
    );
    parentPort.postMessage({ type: "error", workerId, error: err });
  } finally {
    await client.close();
  }
}

indexCSVFragment();